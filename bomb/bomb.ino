
// Libraries
#include <TimedAction.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Keypad.h>
#include <TimedAction.h>


#define ONE_SECOND 1000
#define BUZZER A1
#define BLUELCD 0x38
#define REDDEVICEGREENLCD 0x3F
#define GREENDEVICEGREENLCD 0x3F
#define CODE1 '0'
#define CODE2 '5'
#define CODE3 '1'
#define CODE4 '4'

// Bombs only start beeping when < 45 minutes
#define TIMEBEFOREBEEPING 2700000
#define BEEPAFTERDEATH 30000

//#define LCDADDR 0x27
//#define LCDADDR BLUELCD
#define LCDADDR GREENDEVICEGREENLCD


// Func prototypes
void countdownThread (void);
void decodeThread (void);
void displayCountdownThread (void);

// Reset function hack. 
void(* resetFunc) (void) = 0;//declare reset function at address 0

// Declare Constants
const byte ROWS = 4; //four rows
const byte COLS = 4; //three columns
char keys[ROWS][COLS] = {
  {'0','1','2','3'},
  {'0','4','5','6'},
  {'0','7','8','9'},
  {'0','*','0','#'}
};


//OLD KEYPAD
byte rowPins[ROWS] = {7, 2, 3, 5}; //connect to the row pinouts of the keypad 
byte colPins[COLS] = {12,6, 8, 4}; //connect to the column pinouts of the keypad

//byte rowPins[ROWS] = {7, 2, 3, 5}; //connect to the row pinouts of the keypad 
//byte colPins[COLS] = {10, 8, 4}; //connect to the column pinouts of the keypad


//NEW FLEX KEYPAD 
//byte rowPins[ROWS] = {8, 7, 6, 5}; //connect to the row pinouts of the keypad 
//byte colPins[COLS] = {12,4, 3, 2}; //connect to the column pinouts of the keypad

//byte rowPins[ROWS] = {8, 7, 12, 5}; //connect to the row pinouts of the keypad 
//byte colPins[COLS] = {11, 3, 2}; //connect to the column pinouts of the keypad



//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
//LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
//LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
LiquidCrystal_I2C lcd( LCDADDR , 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

TimedAction countdownTimer = TimedAction(10,countdownThread);
TimedAction decodeAttempt = TimedAction(10,decodeThread);

// Global Variables
long timerValue = 0;

void setup() {
  Serial.begin(9600);
  setupLCD();
  timerValue = 3;
  pinMode(BUZZER, OUTPUT); // Pin for buzzer
  timerValue = getCountdownValue();
  decodeAttempt.disable();
}


void loop() {
  // put your main code here, to run repeatedly:
  //printLCD("Countdown timer", String(countdownTime), 3);
  //Serial.println(countdownTime);
  //delay(2000);
//  long start = millis();
  countdownTimer.check();
//  long difference = millis() - start;
 //Serial.println("Count down timer : " + String(difference));

//  start = millis();
  decodeAttempt.check();
//  difference = millis() -start;
  //Serial.println("Decode attempt timer : " + String(difference));

  decodeAttempt.enable();
  
  


}


void decodeThread (void) {
  //Serial.println("Running decode thread");
  static int count = 0;
  static char code[4] = {'_','_','_','_'}; 
  char key = keypad.getKey();
  Serial.print("Printing key :");
  Serial.println(key);
  //code[0] = '!';
  if (key) {
    acquireScreen();
    Serial.print("New key received :");
    Serial.println(key);
    code[count] = key;
    if ( count < 4 ) {
      count++;
      lcd.setCursor(0, 0);
      lcd.print("   Code :" + String(code[0]) + String(code[1]) + String(code[2]) + String(code[3]) + "   ");
    }
  }

  // Check code
  if (count == 4 ) {

    if ( code[0]== '*' && code[1]== '*' && code[2]== '*' && code[3]== '*' && false ) {
      acquireScreen();
      lcd.setCursor(0, 0);
      lcd.print("Add(*) OR Sub(#)");
      char key = keypad.getKey();
      bool addTime = true;
      while (!key && ((key != '*') || (key != '#'))) {
        
      }
      if (key != '*') {
          addTime = false;
      }
      int time = 0; 
      while (key != '#') {
        lcd.setCursor(0, 0);
        // Check for a star here. 
        time += key - '0';
        lcd.print("Time : " + String(time));
        
      }
      
      
//      releaseScreen();
//      decodeAttempt.disable();
//      count = 0;
//      lcd.print("                ");
//      analogWrite(BUZZER, 0);
//      code[0] = '_';
//      code[1] = '_';
//      code[2] = '_';
//      code[3] = '_';
//      resetFunc();
    }
      


    static long currentTime = 0; 
    static bool firstTime = true;
    bool earlyExit = false;
    if ( passcodeCorrect (code[0],code[1], code[2], code[3]) ) {
      countdownTimer.disable();
      if (executeFor(1500)) {
        lcd.setCursor(0, 0);
        lcd.print("  BOMB DEFUSED ");
        
        if( blinkFraction(150, 500) ) {
          analogWrite(BUZZER, 190);
          
        } else {
          analogWrite(BUZZER, 0);
        }
      } else {
        analogWrite(BUZZER, 0);
        lcd.setCursor(0, 0);
        lcd.print("CONGRATULATIONS!");
        lcd.setCursor(0, 1);
        lcd.print("  ERLA 4 Lyf!!  ");
        count = 0;
        decodeAttempt.disable();
      }
      
    } else {
        
        if (executeFor(3000) ) {
          setSpeedFactor(10);
          if (blinkFraction( 10, 25) ) {
            lcd.setCursor(0, 0);
            lcd.print("  CODE DENIED!  ");
            analogWrite(BUZZER, 190);
            
          } else {
            lcd.setCursor(0, 0);
            lcd.print("                ");
            analogWrite(BUZZER, 0);
            
          }
          currentTime = millis();
          firstTime = true;
         
        
        }
        else {
          
            setSpeedFactor(1);
            lcd.setCursor(0, 0);
            count = 0;
            lcd.print("                ");
            analogWrite(BUZZER, 0);

            code[0] = '_';
            code[1] = '_';
            code[2] = '_';
            code[3] = '_';
            
//            if (code[0] != CODE1 ) {
//              code[0] = '_';
//            }
//           if (code[1] != CODE2 ) {
//              code[1] = '_';
//            }
//            if (code[2] != CODE3 ) {
//              code[2] = '_';
//            }
//           if (code[3] != CODE4 ) {
//              code[3] = '_';
//            }
            releaseScreen();
            decodeAttempt.disable();
          
          
        }
      
    }

  }
  
 
}







void countdownThread (void) {


//  static long lastTime = millis();
//
//  long currentTime = millis();
//  long timeElapsed = currentTime - lastTime; 
//  
//
//  int timeInterval = 10 ;
//
//  static long millisRemaining = (timerValue * 60 * 1000);
//
//  
//  if ( timeElapsed > timeInterval) {
//    lastTime = currentTime - (timeElapsed % timeInterval);
//    millisRemaining = millisRemaining - 100;  
//  }
//
//  long secondsRemaining = millisRemaining / 1000;
//
//  int remainingHours = secondsRemaining / 36000 ;
//  int remainingMinutes = (secondsRemaining % 3600) / 60 ; 
//  int remainingSeconds = (secondsRemaining % 3600) % 60 ; 
//  int remainingMillis = (millisRemaining % 1000)/ 10;



  static long startTime = millis();
  long currentTime = millis();
  static long lastRunTime = currentTime;
  long millisSinceLastRun = currentTime - lastRunTime;

  // Check to see if we have been paused  
  if (currentTime > (lastRunTime + ONE_SECOND) ) {
    // If we have been paused for more than 1 second
    // add the difference to the startTime to negate time spent paused
    startTime += (currentTime - lastRunTime);
    //Serial.println("I have been paused. I will add " + String((currentTime - lastRunTime)));
  }
  
  lastRunTime = currentTime;

  static long cummulativeMillisElapsed = 0;

  // Commented out below to disable the speed up for wrong entry
  //cummulativeMillisElapsed += millisSinceLastRun * getSpeedFactor(); 
  cummulativeMillisElapsed += millisSinceLastRun; 

  
  long millisRemaining = (timerValue * 60 * 1000) -  cummulativeMillisElapsed;
  long secondsRemaining = millisRemaining / 1000;

  //Serial.println("Milliseconds remaining : " + String(millisRemaining) );

  int remainingHours = secondsRemaining / 3600 ;
  int remainingMinutes = (secondsRemaining % 3600) / 60 ; 
  int remainingSeconds = (secondsRemaining % 3600) % 60 ; 
  int remainingMillis = (millisRemaining % 1000)/ 10;
  static bool firstTime = true;

  if (millisRemaining < 0 ) {

    if (firstTime) {
      displayCountdown (0, 0, 0, 0, 0);
      analogWrite(BUZZER, 190);    
      printLCD( "    YOU DIED    ", "    THE END.    ", 0);
      firstTime = false;
    }
      // -----------------
    if (millisRemaining < - BEEPAFTERDEATH) {
      analogWrite(BUZZER, 0);
      countdownTimer.disable();
      decodeAttempt.disable();
    }
      // -----------------
    
 
    return;
  }
  
  displayCountdown (remainingHours, remainingMinutes, remainingSeconds, remainingMillis, millisRemaining);
}


bool screenInUse (bool set, bool usage) {
  static bool screenInUse = false;
  if (set == true) {
    screenInUse = usage;
  }
  return screenInUse;
}

bool checkScreen (void) {
  return screenInUse (false, false);
}

void acquireScreen (void) {
  screenInUse (true, true);
}

void releaseScreen (void) {
  screenInUse (true, false);
}

int speedFactor (bool set, int newSpeed) {
  static int speedFactor = 1;
  if ( set ) {
    speedFactor = newSpeed;
  }
  return speedFactor;
}

void setSpeedFactor (int newSpeed) {
  speedFactor (true, newSpeed);
}

int getSpeedFactor (void) {
  return speedFactor (false, 0);
}



void displayCountdownMessage (long timeRemaining) {

  //Serial.println ("Yes : " + String (timeRemaining)); 
  
  // Determine frequency of blink based on speed 
  int buzzerTimeOn = 100;
  int buzzerTotalTime = 1000;
  int displayTimeOn = 100;
  int displayTotalTime = 1000;
  int speedFactor = getSpeedFactor();


  if ( speedFactor == 1) {
      buzzerTimeOn = 100;
      buzzerTotalTime = 1000;
      displayTimeOn = 100;
      displayTotalTime = 1000;
    } else if (2 <= speedFactor && speedFactor < 4 ) {
      buzzerTimeOn= 100 ;
      buzzerTotalTime = 250 ;
      displayTimeOn = 100;
      displayTotalTime = 250;
    } else {
      buzzerTimeOn = 100 ;
      buzzerTotalTime = 150;
      displayTimeOn = 100;
      displayTotalTime = 250;
    }

  
   if ( timeRemaining < 150000 ) {
    buzzerTimeOn= 100 ;
    buzzerTotalTime = 250 ;
    displayTimeOn = 100;
    displayTotalTime = 250;

    if (timeRemaining < (60000) ) {
      buzzerTimeOn= 100 ;
      buzzerTotalTime = 150 ;
      displayTimeOn = 100;
      displayTotalTime = 250;
    }
   }

   if ( timeRemaining > TIMEBEFOREBEEPING ) { // ADAM HERE. 
    buzzerTimeOn = 0 ;
   }
  //  // Blink the detonation message
  if (! checkScreen() ) {
      // Display
      lcd.setCursor(0, 0);
      if (!blinkFraction( displayTimeOn, displayTotalTime) ) {
        lcd.print(" DETONATING IN:");
      } else {
        lcd.print("                ");
      }
  }
  
  // Buzzer
  if (!blinkFraction( buzzerTimeOn, buzzerTotalTime) ) {
    analogWrite(BUZZER, 0);
  } else {
    analogWrite(BUZZER, 190);
  }

  
}

inline void displayCountdownTimes (int remainingHours, int remainingMinutes, 
                        int remainingSeconds, int remainingMillis) {

    lcd.setCursor(0, 1);
    lcd.print("   ");
  //  if (remainingHours < 10 ) {
  //    //lcd.print("0");
  //  }
    lcd.print(remainingHours);
    lcd.print("h");
    if (remainingMinutes < 10 ) {
      lcd.print("0");
    }
    lcd.print(remainingMinutes);
    lcd.print("m");
    if (remainingSeconds < 10 ) {
      lcd.print("0");
    }
    lcd.print(remainingSeconds);
    lcd.print("s");
     
    if (remainingMillis < 10 ) {
      lcd.print("0");
    }
    lcd.print(remainingMillis);
    lcd.print("");
}

void displayCountdown (int remainingHours, int remainingMinutes, 
                        int remainingSeconds, int remainingMillis, long totalMillisRemaining) {

  displayCountdownMessage(totalMillisRemaining);
  displayCountdownTimes ( remainingHours, remainingMinutes, remainingSeconds, remainingMillis);
}

int getCountdownValue (void) {
  printLCD ("Enter mins then","# to continue", 0); 
  delay(1500);
  printLCD ("Duration : ","                ", 0); 
  
  long countdownValue = 0;
  char key = keypad.getKey();
  int numDigits = 0;
  while ( key != '#' ) {
    if (key && key != '*'){
      lcd.setCursor(numDigits, 1);
      lcd.print(key);
      lcd.print(" min");
      countdownValue = countdownValue * 10 + (key - '0') ;
      numDigits++;
    }
    key = keypad.getKey();
  }
  return countdownValue;
}


bool blinkFraction(int millisOn, int totalMillis) {
  long currentPeriod = millis() % totalMillis;
  if (currentPeriod < millisOn) {
    return true;
  }
  return false;
}

void setupLCD (void) {
  lcd.begin(16,2);   // initialize the lcd for 16 chars 2 lines, turn on backlight
  lcd.setCursor(0,0);
}

// line determines which line is written to. 1 or 2. Other nums in both lines being written.
void printLCD(String line1, String line2, int line) {
  if (line != 2) {
    lcd.setCursor(0, 0);
    lcd.print("                ");
    lcd.setCursor(0, 0);
    lcd.print(line1);
  }
  if (line != 1) {
    lcd.setCursor(0, 1);
    lcd.print("                ");
    lcd.setCursor(0, 1);
    lcd.print(line2);
  }
}



bool executeFor (int duration) {
  long currentTime = millis();
  static long startTime = currentTime;

  long finishTime = startTime + duration; 

  if ( currentTime < finishTime ) {
    //Serial.println("Case 1");
    return true; 
  } else {

    // Check if they want the loop to start, ie 1 second or more since the finish time. 
    if ( currentTime > (finishTime + ONE_SECOND) ) {
      //Serial.println("Case 2");
      //Serial.println("Start time:" + String(startTime));
      //Serial.println("Current time :" +  String(currentTime));
      startTime = currentTime;
      return true;
    } else {
      //Serial.println("Case 3");
      return false;
    }
  }
}

bool passcodeCorrect ( char c1, char c2, char c3, char c4) {
  if (c1 == CODE1 && c2 == CODE2 && c3 == CODE3 && c4 == CODE4) {
    return true;
  }
    return false;
}





